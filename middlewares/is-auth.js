const jwt = require('jsonwebtoken');

const createError = require('../utils/create-error');

module.exports = (req, res, next) => {
    //client will send the token a value of 'Authorization' request header in 'Bearer ...' format,
    // so it should be enabled by CORS Allow-Headers setup
    const authHeader = req.get('Authorization');

    if (!authHeader) {
        throw createError('Not authenticated.', 401, null);
    }

    const token = authHeader.split(' ')[1];
    let decodedToken;

    try {
        decodedToken = jwt.verify(token, process.env.JWT_SECRET);
    } catch (err) {
        throw createError('Not authenticated.', 401, err);
    }

    if (!decodedToken) {
        throw createError('Not authenticated.', 401, null);
    }

    req.userId = decodedToken.userId;
    next();
}
