module.exports = (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*')
        .setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, PATCH')
        .setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
}
