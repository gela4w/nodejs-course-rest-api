const { body } = require('express-validator');

module.exports = [
    body('title')
        .isLength({ min: 3 })
        .withMessage('Post title should contain at least 3 characters.'),
    body('content')
        .isLength({ min: 5 })
        .withMessage('Post title should contain at least 5 characters.')
];
