const { body } = require('express-validator');

const User = require('../models/user');

const signupValidators = [
    body('email')
        .isEmail().withMessage('Please enter a valid email.')
        .custom((value, { req }) => {
            return User.findOne({ email: value }).then(user => {
                if (user) {
                    return Promise.reject('E-mail address already exists.');
                }
            })
        })
        .normalizeEmail(),
    body('name')
        .trim()
        .not().isEmpty().withMessage('Please enter a user name.'),
    body('password')
        .trim()
        .isLength({ min: 6 }).withMessage('Password should contain at least 6 characters.')
];

module.exports = { signupValidators };
