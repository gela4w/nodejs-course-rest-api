let _io;

module.exports = {
    init: httpServer => {
        _io = require('socket.io')(httpServer);
    },
    getIO: () => {
        if (!_io) {
            throw new Error('Socket.io is not initialized.');
        }

        return _io;
    }
}
