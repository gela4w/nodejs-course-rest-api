const { validationResult } = require('express-validator');

const Post = require('../models/post');
const User = require('../models/user');
const createError = require('../utils/create-error');
const deleteFile = require('../utils/delete-file');
const io = require('../configs/socketio');

class FeedController {
    async getPosts(req, res, next) {
        try {
            const { page, perPage } = req.query;
            const totalItems = await Post.find().countDocuments();

            // pagination: don't load the previously loaded items (by skip)
            // and retrieve only certain amount of total set (by limit)
            const posts = await Post.find().skip((page - 1) * perPage).limit(perPage);

            if (!posts) {
                return next(createError('Posts were not found.', 404, null));
            }

            res.status(200).json({ posts, totalItems });
        } catch (err) {
            next(err);
        }
    }

    async getPost(req, res, next) {
        try {
            const { postId } = req.params;
            const post = await Post.findById(postId);

            if (!post) {
                return next(createError('Post was not found.', 404, null));
            }

            res.status(200).json({ post });
        } catch (err) {
            next(err);
        }
    }

    async editPost(req, res, next) {
        const errors = validationResult(req).array();

        if (errors.length) {
            return next(createError('Validation failed.', 422, errors));
        }

        const updatedPost = req.body;

        if (updatedPost.author !== req.userId) {
            return next(createError('Not authorized.', 403, null));
        }

        const { file } = req;

        if (!updatedPost.imageUrl && !file) {
            return next(createError('No image was uploaded.', 422, null));
        }

        try {
            const { postId } = req.params;
            const post = await Post.findById(postId);

            if (!post) {
                return next(createError('Post was not found.', 404, null));
            }

            if (file) {
                deleteFile(post.imageUrl);
                updatedPost.imageUrl = req.file.path;
            } else {
                updatedPost.imageUrl = post.imageUrl;
            }

            Object.assign(post, updatedPost);
            await post.save();

            io.getIO().emit('posts', { action: 'update', post });

            res.status(200).json({ message: 'Post was successfully updated.', post });
        } catch (err) {
            next(err);
        }
    }

    async addPost(req, res, next) {
        const { title, content } = req.body;
        const { file } = req;
        const errors = validationResult(req).array();

        if (errors.length) {
            return next(createError('Validation failed.', 422, errors));
        }

        if (!file) {
            return next(createError('No image was uploaded.', 422, null));
        }

        try {
            const user = await User.findById(req.userId);
            const post = await new Post({ title, content, imageUrl: file.path, author: user._id }).save();

            user.posts.push(post._id);

            await user.save();

            // 'posts' --- event name
            io.getIO().emit('posts', { action: 'create', post });

            res.status(201).json({
                message: 'A new post was added.',
                post,
                author: { id: user._id, name: user.name }
            });
        } catch (error) {
            next(error);
        }
    }

    async deletePost(req, res, next) {
        try {
            const { postId } = req.params;
            const post = await Post.findById(postId);

            if (post.author.toString() !== req.userId) {
                return next(createError('Not authorized.', 403, null));
            }

            if (!post) {
                return next(createError('Post was not found.', 404, null));
            }

            deleteFile(post.imageUrl);

            const user = await User.findById(post.author);

            user.posts = user.posts.filter(postId => !postId.equals(post._id));

            await user.save();
            await Post.deleteOne({ _id: postId });

            io.getIO().emit('posts', { action: 'delete', postId });

            res.status(200).json({ message: 'Post was successfully deleted.' });
        } catch (err) {
            next(err);
        }
    }
}

module.exports = new FeedController();
