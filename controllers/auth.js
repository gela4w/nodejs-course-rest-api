const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { validationResult } = require('express-validator');
const createError = require('../utils/create-error');
const User = require('../models/user');

class AuthController {
    async signup(req, res, next) {
        const errors = validationResult(req).array();

        if (errors.length) {
            return next(createError('Validation failed.', 422, errors));
        }

        try {
            const { email, name, password } = req.body;
            const hashedPassword = await bcrypt.hash(password, 12);

             const user = await new User({
                 email, name, password: hashedPassword, posts: []
             }).save();

            res.status(201).json({ message: 'User was successfully created.', user });
        } catch (err) {
            next(err);
        }
    }

    async login(req, res, next) {
        try {
            const { email, password } = req.body;
            const user = await User.findOne({ email });

            if (!user) {
                return next(createError('Invalid email or password.', 401, null));
            }

            const isPasswordCorrect = await bcrypt.compare(password, user.password);

            if (!isPasswordCorrect) {
                return next(createError('Invalid email or password.', 401, null));
            }

            const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, { expiresIn: '1h' });

            res.status(200).json({
                message: 'Logged in successfully.',
                user: { name: user.name, email: user.email },
                token
            });
        } catch (err) {
            next(err);
        }
    }
}

module.exports = new AuthController();
