const expect = require('chai').expect;

const createError = require('../utils/create-error');

describe('Create Error', () => {
    it('should create an error object with passed params', () => {
        const msg = 'Test message';
        const status = 500;
        const error = 'Error description';

        const err = createError(msg, status, error);

        expect(err.message).to.equal(msg);
        expect(err.statusCode).to.equal(status);
        expect(err._error).to.equal(error);
    });
});
