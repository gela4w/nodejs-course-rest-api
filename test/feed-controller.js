const expect = require('chai').expect;
const sinon = require('sinon');

const Post = require('../models/post');
const User = require('../models/user');
const feedController = require('../controllers/feed');
const io = require('../configs/socketio');
const { req, res, posts, firstPost, firstPostUpdated} = require('../test/mocks/feed');
const nextSpy = sinon.spy();
const nextStub = require('./stubs/next');
const fs = require('fs');

describe('Feed Controller', () => {
    describe('Get Posts', () => {
        it('should return all posts', done => {
            sinon.stub(Post, 'find').callsFake(() => ({
                posts,
                countDocuments: () => Promise.resolve(posts.length),
                skip: function () {
                    return this;
                },
                limit: () => posts,
            }));

            feedController.getPosts(req, res, nextStub).then(() => {
                expect(Post.find.called).to.be.true;
                expect(res.statusCode).to.equal(200);
                expect(res.data.posts).to.equal(posts);
                expect(res.data.totalItems).to.equal(posts.length);
                done();
                Post.find.restore();
            }).catch(err => done(err));
        });

        it('should throw an error if no posts were found', done => {
            sinon.stub(Post, 'find').callsFake(() => ({
                posts: null,
                countDocuments: () => Promise.resolve(null),
                skip: function () {
                    return this;
                },
                limit: () => null,
            }));

            const err = {message: 'Posts were not found.', statusCode: 404, _error: null};

            feedController.getPosts(req, res, nextSpy).then(() => {
                expect(nextSpy.calledWith(sinon.match(err))).to.be.true;
                done();
                Post.find.restore();
            }).catch(err => done(err));
        });
    });

    describe('Get Post', () => {
        it('should return a post by id', done => {
            sinon.stub(Post, 'findById').resolves(firstPost);

            feedController.getPost(req, res, nextStub).then(() => {
                expect(Post.findById.called).to.be.true;
                expect(res.statusCode).to.equal(200);
                expect(res.data.post).to.equal(firstPost);
                done();
                Post.findById.restore();
            }).catch(err => done(err));
        });

        it('should throw an error if no posts with such id was not found', done => {
            sinon.stub(Post, 'findById').resolves(null);

            const err = {message: 'Post was not found.', statusCode: 404, _error: null};

            feedController.getPost(req, res, nextSpy).then(() => {
                expect(nextSpy.calledWith(sinon.match(err))).to.be.true;
                done();
                Post.findById.restore();
            }).catch(err => done(err));
        });
    });

    describe('Edit Post', () => {
        it('should update a post, including uploaded new image', done => {
            sinon.stub(Post, 'findById').resolves({ ...firstPost, save: () => {} });
            sinon.stub(io, 'getIO').returns({ emit: () => {} });
            sinon.stub(fs, 'unlink').callsFake(() => {});

            feedController.editPost(req, res, nextStub).then(() => {
                expect(Post.findById.called).to.be.true;
                expect(res.statusCode).to.equal(200);
                expect(res.data.message).to.equal('Post was successfully updated.');
                expect(res.data.post.title).to.equal(firstPostUpdated.title);
                expect(res.data.post.imageUrl).to.equal(firstPostUpdated.imageUrl);
                done();
                Post.findById.restore();
                io.getIO.restore();
                fs.unlink.restore();
            }).catch(err => done(err));
        });

        it('should throw an error if user is not authorized to update a post', done => {
            const unauthorizedReq = {...req, userId: '6571a787038a49d56ccdc843'};
            const err = {message: 'Not authorized.', statusCode: 403, _error: null};

            feedController.editPost(unauthorizedReq, res, nextSpy).then(() => {
                expect(nextSpy.calledWith(sinon.match(err))).to.be.true;
                done();
            }).catch(err => done(err));
        });

        it('should throw an error if there is no available image', done => {
            const resWithNoImage = { ...req, body: { ...req.body, imageUrl: null }, file: null };
            const err = {message: 'No image was uploaded.', statusCode: 422, _error: null};

            feedController.editPost(resWithNoImage, res, nextSpy).then(() => {
                expect(nextSpy.calledWith(sinon.match(err))).to.be.true;
                done();
            }).catch(err => done(err));
        });
    });

    describe('Add post', () => {
        it('should save a post and link it to its author', done => {
            const user = { _id: '123', name: 'Test', posts: [], save: () => {} };
            sinon.stub(Post.prototype, 'save').resolves(firstPost);
            sinon.stub(User, 'findById').resolves(user);
            sinon.stub(io, 'getIO').returns({ emit: () => {} });

            feedController.addPost(req, res, nextStub).then(() => {
                expect(Post.prototype.save.called).to.be.true;
                expect(res.statusCode).to.equal(201);
                expect(res.data.message).to.equal('A new post was added.');
                expect(res.data.post).to.equal(firstPost);
                expect(res.data.author).to.deep.equal({id: user._id, name: user.name});
                expect(user.posts[0]).to.equal(firstPost._id);
                done();
                Post.prototype.save.restore();
                User.findById.restore();
                io.getIO.restore();
            }).catch(err => done(err));
        });
    });
});
