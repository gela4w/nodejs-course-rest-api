const req = {
    body: {
        email: 'test@test.com',
        password: 'test',
        name: 'Test'
    }
};

const res = {
    statusCode: null,
    data: null,
    status: function (code) {
        this.statusCode = code;
        return this;
    },
    json: function (args) {
        this.data = args;
        return this;
    }
};

const user = { name:  'user' };

module.exports = { req, res, user };
