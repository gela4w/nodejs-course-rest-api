const req = {
    body: {
        email: 'test@test.com',
        password: 'test',
    }
};

const res = {
    statusCode: null,
    data: null,
    status: function (code) {
        this.statusCode = code;
        return this;
    },
    json: function (args) {
        this.data = args;
        return this;
    }
};

const user = { name:  'user', email: 'test@test.com' };

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

module.exports = { req, res, user, token };
