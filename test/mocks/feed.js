const firstPost = {
    title: 'Post N1',
    content: 'First Post',
    _id: '6573065d672a3ac64efa69a1',
    author: '65709b089940c7f5cfe21a47',
    imageUrl: 'public/images/my-image.png'
};

const firstPostUpdated = {
    title: 'Post N1 was updated',
    content: 'New content',
    _id: '6573065d672a3ac64efa69a1',
    author: '65709b089940c7f5cfe21a47'
}

const secondPost = {
    title: 'Post N2',
    content: 'Second Post'
};

const posts = [firstPost, secondPost];

const req = {
    query: {
        page: 1,
        perPage: 2
    },
    params: {
        postId: firstPost._id
    },
    body: firstPostUpdated,
    file: {
        path: 'public/images/my-new-image.png'
    },
    userId: '65709b089940c7f5cfe21a47'
};

const res = {
    statusCode: null,
    data: null,
    status: function (code) {
        this.statusCode = code;
        return this;
    },
    json: function (args) {
        this.data = args;
        return this;
    }
};

module.exports = { req, res, posts, firstPost, firstPostUpdated };
