const expect = require('chai').expect;
const sinon = require('sinon');
const jwt = require('jsonwebtoken');

const isAuth = require('../middlewares/is-auth');
const nextStub = require('./stubs/next');
const { req, res } = require('./mocks/is-auth');

describe('IsAuth', () => {
    it('should throw an error if req does not contain Authorization header', () => {
        const reqWithNoAuthHeader = {
            get: function() {
                return null;
            }
        };

        expect(isAuth.bind(this, reqWithNoAuthHeader, res, nextStub)).to.throw('Not authenticated.');
    });

    it('should decode data from token and insert it to req object', () => {
        //create a stub on OBJECT, METHOD
        sinon.stub(jwt, 'verify').returns({ userId: '123' });

        isAuth(req, res, nextStub);

        expect(req).has.property('userId', '123');

        //NB: don't forget to restore original function
        jwt.verify.restore();
    });

    it('should throw an error if token was not decoded', () => {
        sinon.stub(jwt, 'verify').returns(null);

        expect(isAuth.bind(this, req, res, nextStub)).to.throw('Not authenticated.');

        jwt.verify.restore();
    });
});
