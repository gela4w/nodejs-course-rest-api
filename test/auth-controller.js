const expect = require('chai').expect;
const sinon = require('sinon');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const nextStub = require('../test/stubs/next');
const authController = require('../controllers/auth');
const User = require('../models/user');
const nextSpy = sinon.spy();

describe('Signup', () => {
   it('should create a new user', done => {
       const { req, res, user } = require('../test/mocks/signup');

       sinon.stub(User.prototype, 'save').resolves(user);
       sinon.stub(bcrypt, 'hash').resolves('abc');

       authController.signup(req, res, nextStub).then(() => {
           expect(User.prototype.save.called).to.be.true;
           expect(res.statusCode).to.equal(201);
           expect(res.data.message).to.equal('User was successfully created.');
           expect(res.data.user).to.equal(user);
           done();
           User.prototype.save.restore();
           bcrypt.hash.restore();
       }).catch(err => done(err));
   });
});

describe('Login', () => {
    const { req, res, user, token } = require('../test/mocks/login');

    it('should login a user', done => {
        sinon.stub(User, 'findOne').resolves(user);
        sinon.stub(bcrypt, 'compare').resolves(true);
        sinon.stub(jwt, 'sign').returns(token);

        authController.login(req, res, nextStub).then(() => {
            expect(User.findOne.called).to.be.true;
            expect(jwt.sign.called).to.be.true;
            expect(res.statusCode).to.equal(200);
            expect(res.data.message).to.equal('Logged in successfully.');
            expect(res.data.user).to.deep.equal(user);
            expect(res.data.token).to.equal(token);
            done();
            User.findOne.restore();
            jwt.sign.restore();
            bcrypt.compare.restore();
        }).catch(err => done(err));
    });

    it('should throw an error if user enter incorrect email', done => {
        sinon.stub(User, 'findOne').resolves(null);

        const err = { message: 'Invalid email or password.', statusCode: 401, _error: null };

        authController.login(req, res, nextSpy).then(() => {
            expect(nextSpy.calledWith(sinon.match(err))).to.be.true;
            done();
            User.findOne.restore();
        }).catch(err => done(err));
    });

    it('should throw an error if user enter incorrect password', done => {
        sinon.stub(User, 'findOne').resolves(user);
        sinon.stub(bcrypt, 'compare').resolves(false);

        const err = { message: 'Invalid email or password.', statusCode: 401, _error: null };

        authController.login(req, res, nextSpy).then(() => {
            expect(nextSpy.calledWith(sinon.match(err))).to.be.true;
            done();
            User.findOne.restore();
            bcrypt.compare.restore();
        }).catch(err => done(err));
    });
});
