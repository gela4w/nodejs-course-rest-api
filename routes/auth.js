const express = require('express');

const authController = require('../controllers/auth');
const { signupValidators } = require('../middlewares/auth-validators');

const router = express.Router();

router.post('/signup', ...signupValidators, authController.signup);
router.post('/login', authController.login);

module.exports = router;
