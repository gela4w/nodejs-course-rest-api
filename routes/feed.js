const express = require('express');

const feedController = require('../controllers/feed');
const postValidators = require('../middlewares/post-validators');
const isAuth = require('../middlewares/is-auth');

const router = express.Router();

// http://localhost:3000/feed/posts?page=${page}&perPage=${perPage}
router.get('/posts', isAuth, feedController.getPosts);
router.get('/post/:postId', isAuth, feedController.getPost);
router.post('/add-post', isAuth, ...postValidators, feedController.addPost);
router.put('/edit-post/:postId', isAuth, ...postValidators, feedController.editPost);
router.delete('/delete-post/:postId', isAuth, feedController.deletePost);

module.exports = router;
