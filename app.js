const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
const mongoose = require('mongoose');
require('dotenv').config();

const corsSetup = require('./middlewares/cors-setup');
const feedRoutes = require('./routes/feed');
const authRoutes = require('./routes/auth');
const { storage, fileFilter } = require('./configs/multer');
const socketConfig = require('./configs/socketio');

const app = express();

app.use(bodyParser.json());
app.use(multer({ storage, fileFilter }).single('imageUrl'));
//for any request that goes to path starts with /images ---> serve files statically
app.use('/images', express.static(path.join(__dirname, 'public/images')));
app.use(corsSetup);

app.use('/feed', feedRoutes);
app.use(authRoutes);

app.use((error, req, res, next) => {
    const message = error.message || 'Server error occurred.';
    const status = error.statusCode || 500;
    const _error = error._error || error;

    console.log(_error);
    res.status(status).json({ message });
})

mongoose.connect(process.env.CONNECTION_STRING)
    .then(() => {
        console.log('Successfully connected to MongoDB Server.');

        const server = app.listen(process.env.APP_PORT);

        socketConfig.init(server);

        // event listener, that listens to EVERY client connected to the server
        // and every such connection is stored in socket obj
        socketConfig.getIO().on('connection', socket => {
            console.log('New client is connected to the server!');
        });
    })
    .catch(err => console.log('Connection to MongoDB Server failed with ', err));
