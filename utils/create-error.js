module.exports = (message, statusCode, _error) => {
    const error = new Error(message);
    error.statusCode = statusCode;
    error._error = _error;
    return error;
}
